/*
 Author: Genio - Yüce Özkan
 Author URL: http://www.genio.com.tr/
 */
$(document).ready(function() {

    var isWebkit = 'WebkitAppearance' in document.documentElement.style;
    var html = $(document).find("html");
    var body = $(document).find("body");
    if(isWebkit){html.addClass("webkit");}
    html.addClass("dom-ready");

    window.openingBrowserWidth = $(window).width();
    window.openingBrowserHeight = $(window).height();

    window.time;
    window.idletime = 70;
    $(document).on("mousemove keypress",function(e){
        resetTimer();
    });
    $(window).on("load resize scroll",function(e){
        resetTimer();
    });
    function resetTimer() {
        clearTimeout(window.time);
        window.time = setTimeout(logout, window.idletime*1000);
    }
    function logout() {
        showSubtitle("Benimle ilgilenirmisin canım ciğerim!",1,false,false,'files/test.mp3');
    }

    $(window).load(function(e){
        FastClick.attach(document.body);
    });

    $(window).scroll(function(){

    }).trigger("scroll");

    $(window).resize(function(){
        mobileDetect();
        var newWidth = $(window).width();
        var newHeight = $(window).height();
        if(newWidth > window.openingBrowserWidth)
        {
            showSubtitle("Sayfayı genişlettin bakıyorum?",1,false,false,'files/test.mp3');
        }
        if(newWidth < window.openingBrowserWidth)
        {
            showSubtitle("Sayfayı daraltıyosun bakıyorum?",1,false,false,'files/test.mp3');
        }

        window.openingBrowserWidth = newWidth;
        window.openingBrowserHeight = newHeight;

        if(newWidth < 768 && newWidth > newHeight) // Mobile Landscape
        {
            showSubtitle("Telefonu dik tutabilirmisin tatlım?",1,true,true,'files/test.mp3');

        } else if(newWidth < 768 && newWidth < newHeight) // Mobile Landscape
        {
            enablePage();
        }

        resizeFace();

    }).trigger("resize");


    var shakeEvent = new Shake({threshold: 10});
    shakeEvent.start();
    window.addEventListener('shake', function(){
        //shakeEvent.stop();
        showSubtitle("Ouuuw kafam başın döndü, sallama telefonu",1,false,false,'files/test.mp3');
    }, false);
    if(!("ondevicemotion" in window)){alert("Not Supported");}


    body.cursometer({
        onUpdateSpeed: function (speed) {
            if(Math.ceil(speed) > 6)
            {
                showSubtitle("Ouuuw çok hızlı imlecin var, başım döndü!",1,false,false,'files/test.mp3');
            }
        },
        updateSpeedRate: 100,
        captureMouseMoveRate: 200
    });

    $(document).on("click","#faceDetails",function(e){
        if($(e.target).attr("id") == "search" || $(e.target).parents("#search").length > 0)
        {

        }
        else
        {
            e.preventDefault();
            if(!body.hasClass("mimik"))
            {
                var posX = $(this).offset().left,
                    posY = $(this).offset().top,
                    targetWidth = $(this).width(),
                    targetHeight = $(this).height();
                var left = e.pageX - posX;
                var top = e.pageY - posY;
                var leftPercentage = Math.ceil(left/targetWidth*100);
                var topPercentage = Math.ceil(top/targetHeight*100);

                if(
                    (leftPercentage >= 0 && leftPercentage <= 17) &&
                    (topPercentage >= 37 && topPercentage <= 90)
                )
                {
                    showSubtitle("Benim sol yanağım yalnız o!",1,false,false,'files/test.mp3');
                }
                if(
                    (leftPercentage >= 84 && leftPercentage <= 100) &&
                    (topPercentage >= 37 && topPercentage <= 90)
                )
                {
                    showSubtitle("Benim sağ yanağım yalnız o!",1,false,false,'files/test.mp3');
                }
                if(
                    (leftPercentage >= 20 && leftPercentage <= 25) &&
                    (topPercentage >= 21 && topPercentage <= 38)
                )
                {
                    showSubtitle("Benim sol gözüm yalnız o!",1,false,false,'files/test.mp3');
                }
                if(
                    (leftPercentage >= 75 && leftPercentage <= 80) &&
                    (topPercentage >= 21 && topPercentage <= 38)
                )
                {
                    showSubtitle("Benim sağ gözüm yalnız o!",1,false,false,'files/test.mp3');
                }
                if(
                    (leftPercentage >= 30 && leftPercentage <= 70) &&
                    (topPercentage >= 45 && topPercentage <= 80)
                )
                {
                    showSubtitle("Benim burnum yalnız o!",1,false,false,'files/test.mp3');
                }
                console.log(leftPercentage,topPercentage);
            }
        }

    });


    $(document).on("submit","form#search",function(e){

        e.preventDefault();
        var val = $("input",this).val();
        if(val.length > 0)
        {
            var kelimeler = val.split(" ");
            var heceler = [];
            $.each(kelimeler,function(index,kelime){
                if(index > 0)
                {
                    heceler.push("+");
                }

                kelime.replace(/[^\x00-\x7F]/g, "");
                kelime.replace(/[^/\"_+-=a-zA-Z 0-9]+/g,'');

                var len = kelime.length-1;
                for(var i=0;i<=len;i++)
                {
                    var hece = kelime.substring(i,i+parseInt(2));
                    if(hece.length == 2)
                    {
                        heceler.push(hece);
                    }
                }
            });

            console.log(kelimeler,heceler);
            showSubtitle(val,10,false,false,'files/test.mp3');
        }

    });


    function enablePage(){
        $(document).find("form#search").show();
    }

    function mobileDetect(){
        var html = $(document).find("html");
        var md = new MobileDetect(window.navigator.userAgent);
        html.removeClass("mobile phone tablet");
        if(md.mobile())
        {
            html.addClass("mobile");
        }
        if(md.phone())
        {
            html.addClass("phone");
        }
        if(md.tablet())
        {
            html.addClass("tablet");
        }

        if(html.hasClass("mobile")){
        }
        else
        {
        }
    }


});//ready

function stopmp3(){
    var audioElementDom = $(document).find("audio#audio"),
        audioElement = $(document).find("audio#audio")[0];
    audioElement.currentTime = 0;
    audioElement.pause();
    audioElementDom.removeAttr("src");
}
function resizeFace(){
    var newWidth = $(window).width();
    var faceDetails = $(document).find("#faceDetails");
    if(faceDetails.find("svg").length > 0)
    {
        faceDetails.find("svg").remove();
    }
    if(newWidth < 768)
    {
        var cssBackground = faceDetails.css("background-image");
        cssBackground = cssBackground.replace('url("data:image/svg+xml;utf8,','');
        cssBackground = cssBackground.replace('")','');
        cssBackground = cssBackground.replace(/\\"/g, '"');
        faceDetails.append(cssBackground);

        var search = $(document).find("#search");
        var input = $(document).find("#search label input");
        var button = $(document).find("#search button");
        var faceDetailsHeight = faceDetails.find("svg").height();
        var faceDetailsTop = (faceDetailsHeight/2) + parseInt(40);
        faceDetails.css({
            "height":faceDetailsHeight+"px",
            "margin-top":"-"+faceDetailsTop+"px"
        });
    }
    else {

        faceDetails.css({
            "height":"",
            "margin-top":""
        });
    }
}
function showSubtitle(val,mimikId,disable,fixed,mp3url){
    var body = $(document).find("body");
    var len = val.length*40;
    var seconds = parseInt(len) + parseInt(2000);

    var audioElementDom = $(document).find("audio#audio"),
        audioElement = $(document).find("audio#audio")[0];
    audioElementDom.attr("src",mp3url);
    audioElement.load();
    audioElement.volume = 1;
    audioElement.addEventListener("canplay", function() {
        audioElement.play();
        initEffect(val,mimikId,disable,fixed);
    });

    function initEffect(val,mimikId,disable,fixed){
        if(mimikId > 0)
        {
            body.addClass("mimik");
            body.addClass("mimik-"+mimikId);
            resizeFace();
        }

        if(mimikId == 1) // Kızgın
        {
        }
        if(mimikId == 2) // Anlamamış
        {
        }
        if(mimikId == 3) // Bilememiş
        {
        }
        if(mimikId == 4) // Bilememiş 2
        {
        }
        if(mimikId == 5) // Bilmiş
        {
        }
        if(mimikId == 6) // Bilmiş 2
        {
        }
        if(mimikId == 7) // Bilmiş 3
        {
        }
        if(mimikId == 8) // Ciddi
        {
        }
        if(mimikId == 9) // Ehe Ehe İfadeli
        {
        }
        if(mimikId == 10) // Keyifli
        {
        }

        $(document).find("form#search").hide();
        $(document).find("#subtitle").show().html('<span>'+val+'</span>');

        setTimeout(function(){

            if(!fixed)
            {
                $(document).find("form#search input").val("").focus().click();
                $(document).find("#subtitle").hide().html("");
                if(disable)
                {
                    $(document).find("form#search").hide();
                }
                else {
                    $(document).find("form#search").show();
                }
                if(mimikId > 0)
                {
                    body.removeClass("mimik");
                    body.removeClass("mimik-"+mimikId);
                    resizeFace();
                }
            }

            stopmp3();

        },seconds)
    }



}
